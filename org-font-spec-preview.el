;;; org-font-spec-preview.el --- font preview and opentype features test using org-latex-preview    -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2024  Juan Manuel Macías

;; Author: Juan Manuel Macías <maciaschain@posteo.net>
;; URL: https://gitlab.com/maciaschain/org-font-spec-preview
;; Version: 0.9
;; Package-Requires: ((emacs "27.2") (org "9.4"))
;; Keywords: org-mode, fonts, opentype, typefaces, LaTeX, LuaTeX, typography

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

  ;;; Code:

(provide 'org-font-spec-preview)

(require 'button)
(require 'ido)

(defvar ofsp-otf-features-list (regexp-opt '( "abvf"   "abvm"   "abvs"   "aalt"   "akhn"
     "nalt"   "halt"   "afrc"   "valt"   "vhal"   "blwf"   "blwm"   "blws"   "cpsp"   "c2pc"   "c2sc"
     "case"   "cpct"   "cv01"   "cv02"   "cv03"   "cv04"   "cv05"   "cv06"   "cv07"   "cv08"   "cv09"
     "cv10"   "cv11"   "cv12"   "cv13"   "cv14"   "cv15"   "cv16"   "cv17"   "cv18"   "cv19"   "cv20"
     "cv21"   "cv22"   "cv23"   "cv24"   "cv25"   "cv26"   "cv27"   "cv28"   "cv29"   "cv30"   "cv31"
     "cv32"   "cv33"   "cv34"   "cv35"   "cv36"   "cv37"   "cv38"   "cv39"   "cv40"   "cv41"   "cv42"
     "cv43"   "cv44"   "cv45"   "cv46"   "cv47"   "cv48"   "cv49"   "cv50"   "cv51"   "cv52"   "cv53"
     "cv54"   "cv55"   "cv56"   "cv57"   "cv58"   "cv59"   "cv60"   "cv61"   "cv62"   "cv63"   "cv64"
     "cv65"   "cv66"   "cv67"   "cv68"   "cv69"   "cv70"   "cv71"   "cv72"   "cv73"   "cv74"   "cv75"
     "cv76"   "cv77"   "cv78"   "cv79"   "cv80"   "cv81"   "cv82"   "cv83"   "cv84"   "cv85"   "cv86"
     "cv87"   "cv88"   "cv89"   "cv90"   "cv91"   "cv92"   "cv93"   "cv94"   "cv95"   "cv96"   "cv97"
     "cv98"   "cv99"   "cjct"   "cfar"   "calt"   "clig"   "cswh"   "curs"   "dnom"   "dlig"   "dist"
     "expt"   "falt"   "frac"   "fwid"   "ccmp"   "haln"   "half"   "hwid"   "hngl"   "hist"   "hlig"
     "hkna"   "hojo"   "init"   "isol"   "ital"   "jp04"   "jp78"   "jp83"   "jp90"   "jalt"   "kern"
     "lnum"   "ljmo"   "lfbd"   "ltra"   "ltrm"   "locl"   "mark"   "mset"   "mkmk"   "mgrk"   "medi"
     "med2"   "nlck"   "nukt"   "numr"   "onum"   "opbd"   "size"   "ordn"   "ornm"   "pcap"   "pref"
     "pres"   "psts"   "pstf"   "palt"   "vpal"   "pnum"   "pkna"   "pwid"   "qwid"   "rand"   "rkrf"
     "rphf"   "rlig"   "rtbd"   "rtla"   "rtlm"   "ruby"   "sinf"   "smpl"   "zero"   "smcp"   "liga"
     "salt"   "ss01"   "ss02"   "ss03"   "ss04"   "ss05"   "ss06"   "ss07"   "ss08"   "ss09"   "ss10"
     "ss11"   "ss12"   "ss13"   "ss14"   "ss15"   "ss16"   "ss17"   "ss18"   "ss19"   "ss20"   "subs"
     "sups"   "swsh"   "tnum"   "fina"   "fin2"   "fin3"   "twid"   "titl"   "trad"   "tnam"   "tjmo"
     "unic"   "vatu"   "vert"   "vrt2"   "vkna"   "vkrn"  "vjmo")))

(defvar ofsp-font-specimen-dir (when load-file-name
			       (f-slash (f-join (f-parent load-file-name) ""))))

(defvar ofsp-options-list '((?1 "Glyph(s)" (lambda ()
							     (interactive)
							     (format "%s" (read-from-minibuffer "Insert a string: "))))
					    (?2 "Code" (lambda ()
							 (interactive)
							 (let ((chars (read-from-minibuffer
								       "Insert character(s) by code (i.e. \"006f\", 1 space between each code; \"|\" = word sep.): ")))
							   (replace-regexp-in-string
							    "|" " "
							    (replace-regexp-in-string
							     "\s*\\([[:alnum:]]+\\)\s*" "{^^^^\\1}"
							     chars)))))
					    (?3 "Print specimen" (lambda ()
								   (interactive)
								   (with-temp-buffer
								     (insert-file-contents (concat ofsp-font-specimen-dir "specimen.tex"))
								     (buffer-string))))))

;;;###autoload
(defun ofsp-choice ()
  (interactive)
  (let ((opt (read-char-choice (concat "Preview code or string >>\n\n"
				       (mapconcat (lambda (item)
						    (format "%c: %s"
							    (car item) (nth 1 item)))
						  ofsp-options-list " --- "))
			       (mapcar #'car ofsp-options-list))))
    (funcall (nth 2
		  (assoc
		   opt
		   ofsp-options-list)))))

;;;###autoload
(defun ofsp--run-preview (file &optional string)
  "TODO"
  (let* ((font (file-name-nondirectory file))
	 (path (file-name-directory file))
	 (specs-otfinfo
	  (split-string
	   (shell-command-to-string
	    (concat "otfinfo -f "
		    (shell-quote-argument file)))
	   "\n"))
	 (string (if string string (ofsp-choice)))
	 (size (read-from-minibuffer "Font size in points (default 18pt): " nil nil t t "18"))
	 (interline (read-from-minibuffer "Interline in points (default 21.6pt): " nil nil t t "21.6"))
	 (opentype-feature (read-from-minibuffer "Opentype features (default none): " nil nil t t "[]"))
	 (doc (concat
	       "\n\\begin{null}\n"
	       (format "\\fontsize{%spt}{%spt}\\selectfont\n" size interline)
	       string
	       "\n\\end{null}\n")))
    (setq org-format-latex-header (concat "\\documentclass[varwidth]{standalone}\n"
					  "\\usepackage{xcolor}\n"
					  "\[PACKAGES]
					  \[DEFAULT-PACKAGES]"
					  "\n\\pagestyle{empty}"
					  "\n\\usepackage{fontspec}"
					  "\n\\usepackage[spanish,english]{babel}"
					  "\n\\setmainfont{"
					  font
					  "}[Path="
					  path
					  (if (not (equal opentype-feature "[]"))
					      (format ",RawFeature={%s}" opentype-feature)
					    "")
					  "]"
					  "\\pagestyle{empty}"))
    (when (get-buffer "*font-preview*")
      (kill-buffer "*font-preview*"))
    (get-buffer-create "*font-preview*")
    (set-buffer "*font-preview*")
    (org-mode)
    (mapc (lambda (el)
	    (let ((spec (when (string-match (concat "\\(" ofsp-otf-features-list "\\)") el)
			  (match-string 1 el))))
	      (insert-button (format "%s\n" el)
			     'action (lambda (x)
				       (org-clear-latex-preview (point-min) (point-max))
				       (goto-char (point-min))
				       (re-search-forward "\\(\\\\begin{null}\\)\\(\n\\\\addfontfeature.+\\)?" nil t)
				       (replace-match (concat "\\1\n\\\\addfontfeature{RawFeature=" spec "}") t nil)
				       (org-latex-preview)))))
	  specs-otfinfo)
    (insert "\n\n")
    (insert doc)
    (org-latex-preview)
    (pop-to-buffer "*font-preview*")
    (goto-char (point-min))
    (setq header-line-format (format "Preview: %s" font))))

;;;###autoload
(defun ofsp-make-system-fonts-list ()
  (let ((str (shell-command-to-string "fc-list")))
    (split-string str "\n")))

;;;###autoload
(defun ofsp-system-fonts-list (arg)
  (interactive  (list (ido-completing-read "Fuentes en el sistema: " (ofsp-make-system-fonts-list) nil t)))
  (when (string-match "\\(.+\\.otf\\|.+\\ttf\\).+" arg)
    (setq ofsp-system-font-candidate (match-string 1 arg))))

;;;###autoload
(defun ofsp-system-font-candidate-format ()
  (interactive)
  (call-interactively 'ofsp-system-fonts-list)
  (format "%s" ofsp-system-font-candidate))

;;;###autoload
(defun ofsp-system-font ()
  (interactive)
  (ofsp--run-preview (funcall 'ofsp-system-font-candidate-format)))

;;;###autoload
(defun ofsp-dired-font ()
  (interactive)
  (if (not (derived-mode-p 'dired-mode))
      (error "Not in dired!")
    (let ((file (dired-get-file-for-visit)))
      (ofsp--run-preview file))))
