;;; helm-albatross.el --- a Helm extension for the Albatross TeX Live command line tool -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Juan Manuel Macías

;; Author: Juan Manuel Macías <maciaschain@posteo.net>
;; URL: https://gitlab.com/maciaschain/org-font-spec-preview
;; Version: 0.9
;; Package-Requires: ((emacs "27.2") (org "9.4"))
;; Keywords: Helm, Albatross, org-mode, fonts, opentype, typefaces, LaTeX, LuaTeX, typography

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

  ;;; Code:

(provide 'helm-albatross)

(require 'org-font-spec-preview)
(require 'helm)

(defvar helm-albatross-list nil)
(defvar helm-albatross-candidates-list nil)
(defvar helm-albatross-candidate nil)
(defvar helm-albatross-source nil)

(defun helm-albatross--make-candidates (str)
  (setq helm-albatross-list nil)
  (let ((raw-list (shell-command-to-string
		   (concat
		    "albatross "
		    str
		    " --border-style 0 --detailed --show-styles --include-tex-fonts"))))
    (with-temp-buffer
      (insert raw-list)
      (save-excursion
	(goto-char (point-min))
	(while (re-search-forward "\\(^-\s+\\)\\(.+\\.otf\\|.+\\.ttf\\)" nil t)
	  (let* ((path (save-excursion (re-search-backward "\\(^-\s+\\)\\(.+\\)" nil t )
				       (buffer-substring-no-properties (match-beginning 2) (match-end 2))))
		 (name (save-excursion (re-search-backward "\\(Name:\s+\\)\\(.+\\)" nil t)
				       (buffer-substring-no-properties (match-beginning 2) (match-end 2))))
		 (cons (cons name path)))
	    (push cons helm-albatross-list)))))
    (let* ((fonts-list (mapcar (lambda (x)
				 (let ((name (car x))
				       (path (cdr x)))
				   (format "%s: %s" name path)))
			       helm-albatross-list)))
      (setq helm-albatross-candidates-list fonts-list))))

(setq helm-albatross--actions
      (helm-make-actions
       "org-font-spec-preview with chosen font" (lambda (font)
						  (let ((ruta (when (string-match ":\s+\\(.+\\)" font)
								(match-string 1 font))))
						    (ofsp--run-preview
						     (expand-file-name ruta)
						     helm-albatross-candidate)))

       "Visit the font directory" (lambda (font)
				    (let* ((full-path (when (string-match ":\s+\\(.+\\)" font)
							(match-string 1 font)))
					   (path (file-name-directory full-path))
					   (file (file-name-nondirectory full-path)))
				      (find-file path)
				      (goto-char (point-min))
				      (re-search-forward file nil t)))))

(defun helm-albatross ()
  (interactive)
  (setq helm-albatross-candidate nil)
  (let ((candidate (read-from-minibuffer "Character(s): ")))
    (helm-albatross--make-candidates candidate)
    (setq helm-albatross-candidate candidate)
    (setq helm-albatross-source
	  (let* ((format-str (format "Fonts including %s" helm-albatross-candidate))
		 (list `((name . ,format-str)
			  (action . helm-albatross--actions)
			  (candidates . helm-albatross-candidates-list))))
	    list))
    (helm :sources '(helm-albatross-source))))
